//
//  ViewController.swift
//  ShadowTableView
//
//  Created by Hitendra on 13/01/21.
//  Copyright © 2021 Hitendra. All rights reserved.
//

import UIKit

class CustomCell : UITableViewCell {
    
    @IBOutlet weak var mainView : UIView?
    @IBOutlet weak var bodyView : UIView?
    @IBOutlet weak var lblText : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.mainView.dropShadow()
//        self.mainView.layer.cornerRadius = 6.0
//        self.bodyView.layer.borderWidth = 0.5
//        self.bodyView.layer.borderColor = UIColor.gray.cgColor
//        self.bodyView.layer.cornerRadius = 6.0
//        self.bodyView.layer.masksToBounds = true
        self.dropShadow()
        self.mainView!.layer.borderWidth = 0.5
        self.mainView!.layer.borderColor = UIColor.gray.cgColor
        self.mainView!.layer.cornerRadius = 6.0
        self.mainView!.layer.masksToBounds = true

      
        
        
//        self.mainView.layer.cornerRadius = 6.0
//        self.mainView.layer.borderWidth = 0.5
//        self.mainView.layer.borderColor = UIColor.gray.cgColor
//        self.mainView.layer.cornerRadius = 6.0
//        self.mainView.layer.masksToBounds = true

        
    }

}
class ViewController: UIViewController {

    @IBOutlet weak var tblview : UITableView!
    var arrayTexts = ["sdfsdfsdfas","sdfsadfsdfsadfsadfsdafsadfsdfdsfsadfsad","sdfasdfadfa"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}
extension ViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayTexts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomCell
        cell?.lblText?.text = self.arrayTexts[indexPath.row]
        
        return cell!
    }
    
    
}

extension UIView {
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.4
    }
}
